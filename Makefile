APPS     := init registry

all:	$(APPS)

$(APPS):
	go build -o bin/echo-server main.go

clean:
	rm -rf bin/*

dep: 
	go get -v -d ./...
