ARG VERSION=1.18

FROM golang:${VERSION} AS builder

WORKDIR /build
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
COPY . .

RUN make

# Docker Image

FROM alpine:3.13
WORKDIR echo-server
# Create a echo-server user and group first so the IDs get set the same way,
# even as the rest of this may change over time.
RUN addgroup echo-server && \
  adduser -S -G echo-server echo-server

COPY --from=builder /build/bin/echo-server /bin/echo-server

# /echo-server/logs is made available to use as a location to store audit logs, if
# desired; /echo-server/file is made available to use as a location with the file
# storage backend, if desired; the server will be started with /echo-server/config as
# the configuration directory so you can add additional config files in that
# location.
RUN mkdir -p /echo-server/config && chown -R echo-server:echo-server /echo-server

# 8200/tcp is the primary interface that applications use to interact with
# echo-server.
EXPOSE 8080

CMD ["echo-server"]
